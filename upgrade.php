<?php

/**
 *  @module      	Library Bootstrap
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2014-2024 CMS-LAB
 *  @license        http://opensource.org/licenses/MIT
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {
    include LEPTON_PATH.SEC_FILE;
} else {
    $oneback = "../";
    $root = $oneback;
    $level = 1;
    while (($level < 10) && (!file_exists($root.SEC_FILE))) {
        $root .= $oneback;
        $level += 1;
    }
    if (file_exists($root.SEC_FILE)) {
        include $root.SEC_FILE;
    } else {
        trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
    }
}
// end include secure.php

//  remove unneeded files 
if (!function_exists('rm_full_dir')) {
    include_once LEPTON_PATH.'/framework/functions/function.rm_full_dir.php';
}

if (file_exists (LEPTON_PATH.'/modules/lib_bootstrap/bootstrap/fonts/index.php')) {	
		rm_full_dir( LEPTON_PATH.'/modules/lib_bootstrap/bootstrap/fonts' ); 
}

$temp_path = LEPTON_PATH ."/modules/lib_bootstrap/bootstrap/css/";
$to_delete = array(
		"bootstrap-theme.css",
		"bootstrap-theme.css.map",
		"bootstrap-theme.min.css",
		"bootstrap-theme.min.css.map",
		"bootstrap-reboot.min.css",		
		"bootstrap-reboot.min.css.map",
		"bootstrap-reboot.css",
		"bootstrap-reboot.css.map",
		"bootstrap-grid.css",
		"bootstrap-grid.css.map",
		"bootstrap-grid.min.css",
		"bootstrap-grid.min.css.map"
);
		
foreach ($to_delete as $ref)  {	
	if (file_exists($temp_path.$ref)) {
		$result = unlink ($temp_path.$ref);
		if (false === $result) {
			echo "Cannot delete file ".$ref.". Please check file permissions and ownership or delete file manually.";
		}
	}
}

