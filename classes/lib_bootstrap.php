<?php

/**
 *  @module      	Library Bootstrap
 *  @version        see info.php of this module
 *  @author         cms-lab
 *  @copyright      2014-2024 CMS-LAB
 *  @license        http://opensource.org/licenses/MIT
 *  @license terms  see info.php of this template
 *  @platform       see info.php of this template
 */


class lib_bootstrap extends LEPTON_abstract
{

	// Inherited function from LEPTON_abstract.
    public function initialize()
    {

    }
	
}	

